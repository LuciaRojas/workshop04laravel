# Workshop 04 - Laravel From The Scratch (3° Entregable)Tarea


Primero que todo se levanta  vagrant con el siguiente comando `vagrant up` 


luego se el siguiente comando `vagrant rsync-auto`  

Luego entraremos donde tenemos el proyecto y lo abrimos para trabajar 
 
![coamdo tree](01.jpg "comando tree")

## Laravel From The Scratch - 10 Mail
# 42.Send Raw Mail

La forma más fácil de enviar un correo electrónico en Laravel es con el método Mail :: raw (). En esta lección, aprenderemos cómo enviar un formulario, leer una dirección de correo electrónico proporcionada en la solicitud y luego enviar un correo electrónico a la persona.


![coamdo tree](02.jpg "comando tree")
En la carpte de `routes`, nos vamos al archivo de `web.php`, donde simplemente copiamos la ruta que tenemos `get` y pegamos abajo donde le cambiaremos el get para obtener datos a un  `post` para recibir datis y le diremos que va al fuction `store` 
![coamdo tree](03.jpg "comando tree")

En el controller de `ContactController.php` vamos a crear una nueva funcion la cual se llamara `store` la cual sirvira para enviar el `email`
![coamdo tree](04.jpg "comando tree")

En la vista el method la accion a realizar sera un  `POST` donde  la ruta `contact` par enviar el correo, lo que recibira seria el correo
![coamdo tree](05.jpg "comando tree")
![coamdo tree](06.jpg "comando tree")
en el formulario en la vista se coloca una validacion que cuando llegue al controller y si le devuelve un error que lo muestre
![coamdo tree](07.jpg "comando tree")
en la fuction de `store` en controller validaremos que el correo que uno envie sea correo y no simple texto
![coamdo tree](08.jpg "comando tree")
y este seria el error que le mostraria si envia solo texto y no un correo
![coamdo tree](09.jpg "comando tree")
se utiliza la funcion que laravel tiene la de `Mail::raw` donde se le enviara al correo con el mensaje dijitado 
![coamdo tree](10.jpg "comando tree")

en el archivo `.env` se modifica la parte en la direccion de correo que seria del que va enviar al correo  `MAIL_FROM_ADDRESS=correo `
![coamdo tree](11.jpg "comando tree")


En esta parte se muestra un historial de los correos enviados `storage/logs/laravel`
![coamdo tree](12.jpg "comando tree")


Si el mensaje se envia se le enviara un mensaje de `Email sent`
![coamdo tree](13.jpg "comando tree")
se coloca en la vissta donde quiere que se muestre el mensaje 
![coamdo tree](14.jpg "comando tree")

![coamdo tree](15.jpg "comando tree")


# 43.Simulate an Inbox using Mailtrap
cualquier correo que se envía mientras está en modo de desarrollo, pero cambiemos al uso de Mailtrap. Esto nos permitirá simular una bandeja de entrada de correo electrónico de la vida real, que será especialmente útil una vez que comencemos a enviar correo electrónico HTML
utilizaremos `mailtrap` 
![coamdo tree](16.jpg "comando tree")
Nos rejistraremos use `Google account`
![coamdo tree](17.jpg "comando tree")
Escribimos `Demo` y ledamos `Create inbox`
![coamdo tree](18.jpg "comando tree")
Utilizaremosla informacion que nos dan
![coamdo tree](19.jpg "comando tree")

rellenamos los campos con los datos que nos han dado
![coamdo tree](20.jpg "comando tree")

Asi que cada vez que enviamos un correo podremos verlo
![coamdo tree](21.jpg "comando tree")


# 44.Send HTML Emails Using Mailable Classes
Hasta ahora, solo hemos logrado enviar un correo electrónico básico en texto plano. Actualicemos a un correo electrónico completo basado en HTML aprovechando las clases de correo electrónico de Laravel.

`App/contact-me.php` cambiaremos la vista a `emails.contact-me` y creamos una nueva vista  de `contact-me.php`
![coamdo tree](22.jpg "comando tree")

![coamdo tree](23.jpg "comando tree")
en store haremos algunos cambios que seran los siguientes
![coamdo tree](24.jpg "comando tree")
![coamdo tree](25.jpg "comando tree")

# 45.Send Email Using Markdown Templates
Podemos escribir correos electrónicos usando Markdown. En esta lección, aprenderá a enviar correos electrónicos con un formato agradable construido por el marco. Para los casos en los que necesite modificar la estructura HTML subyacente, también publicaremos los activos que se pueden enviar por correo y revisaremos cómo crear temas personalizados.

como en este casi  crearemos una prueba y la enviaremos
![coamdo tree](26.jpg "comando tree")
asi se veria en el correo
![coamdo tree](27.jpg "comando tree")
ahora correremos todo el texto
![coamdo tree](28.jpg "comando tree")
y como se puede ver la letra se cambio y tambien las figuras
![coamdo tree](29.jpg "comando tree")

# 46.Notifications Versus Mailables
Hemos recurrido exclusivamente a las clases Mailable para enviar correos electrónicos; sin embargo, existe un enfoque alternativo que también podría considerar. Una clase de notificación se puede utilizar para notificar a un usuario en respuesta a alguna acción que realizó en su sitio web. La diferencia está en cómo se notifica al usuario. Claro, podemos enviarles un correo electrónico, pero también podemos notificarles a través de un mensaje de texto, una notificación de Slack, ¡o incluso como una tarjeta postal física!
![coamdo tree](31.jpg "comando tree")
![coamdo tree](32.jpg "comando tree")


## Laravel From The Scratch - 11 Notifications

# 47.Database Notifications

Una notificación puede enviarse a través de cualquier número de "canales". Quizás una notificación en particular debería alertar al usuario por correo electrónico y a través del sitio web. ¡Claro, no hay problema! Aprendamos cómo en este episodio.

Se crea un esquema para exportar los datos a la base de datos
![coamdo tree](33.jpg "comando tree")

![coamdo tree](34.jpg "comando tree")
![coamdo tree](35.jpg "comando tree")
![coamdo tree](36.jpg "comando tree")
![coamdo tree](37.jpg "comando tree")

# 48.Send SMS Notifications in 5 Minutes
Para este próximo canal de notificación, elegiremos uno que nunca he usado personalmente: mensajes SMS. Como verá, incluso sin experiencia previa, es ridículamente simple enviar mensajes de texto condicionalmente a los usuarios de su aplicación.

Vamo ayudarnos con la siguiente pagina
![coamdo tree](38.jpg "comando tree")
Vamo a ir a la parte donde dice `Try Free` y nos motrara la siguiente pantalla
![coamdo tree](39.jpg "comando tree")
luego colocamos el siguiente comando 
![coamdo tree](40.jpg "comando tree")
vamos a ir estan nuestros datos personales `Api Key` y la `Api secret`
![coamdo tree](41.jpg "comando tree")
luego vamos a ir ar archivo Vamo a ir a la parte donde dice `Try Free` y nos motrara la siguiente pantalla `.env`
![coamdo tree](42.jpg "comando tree")
donde colococaremos los datos que nos dan
![coamdo tree](43.jpg "comando tree")
luego vamos a ir a esta parte y seleccionamos `Yur numbers`
![coamdo tree](44.jpg "comando tree")
nos mostrara los numeros de telefonos que tenemos 
![coamdo tree](45.jpg "comando tree")
seleccionamos el que queremos y lo copiamos 
![coamdo tree](46.jpg "comando tree")
para luego copiarlo en este archivo
![coamdo tree](47.jpg "comando tree")
exactamente en esta parte 
![coamdo tree](48.jpg "comando tree")
![coamdo tree](49.jpg "comando tree")
![coamdo tree](50.jpg "comando tree")

## Laravel From The Scratch - 12 Events
# 49.Eventing Pros and Cons

Los eventos ofrecen una forma para que una parte de su aplicación haga un anuncio que se propague por todo el sistema. 

![coamdo tree](51.jpg "comando tree")
![coamdo tree](52.jpg "comando tree")
![coamdo tree](53.jpg "comando tree")


## Laravel From The Scratch - 13 Authorizations
# 50.Limit Access to Authorized Users

Algunas acciones deben limitarse a usuarios autorizados. Quizás solo el creador de una conversación puede seleccionar qué respuesta responde mejor a su pregunta. Si este es el caso, necesitaremos escribir la lógica de autorización necesaria.

agregaremos un campo mas a la tabla 
![coamdo tree](54.jpg "comando tree")
migramos la tabla 
![coamdo tree](55.jpg "comando tree")
pondremos el conseration para tener un auotrizacion
![coamdo tree](56.jpg "comando tree")

![coamdo tree](57.jpg "comando tree")
se puede ver el boton
![coamdo tree](58.jpg "comando tree")
pondremos lo sigyuiente para que no cualquier usuario pueda ver 
![coamdo tree](59.jpg "comando tree")
y refrescamos la pagina y ahora deberia de salirnos asi.
![coamdo tree](60.jpg "comando tree")
![coamdo tree](61.jpg "comando tree")

# 51.Filtros de autorización
Finalmente podemos pasar a las fachadas de Laravel, que proporcionan una interfaz estática conveniente para todos los componentes subyacentes del marco. En esta lección, revisaremos la estructura básica, cómo rastrear la clase subyacente y cuándo puede optar por no usarlas.

![coamdo tree](62.jpg "comando tree")
Laravel incluye varios filtros por defecto, uno de ellos es el encargado de realizar la autenticación de los usuarios. Este filtro lo podemos aplicar sobre una ruta, un conjunto de rutas o sobre un controlador en concreto. Este middleware se encargará de filtrar las peticiones a dichas rutas: en caso de estar logueado y tener permisos de acceso le permitirá continuar con la petición, y en caso de no estar autenticado lo redireccionará al formulario de login.
![coamdo tree](63.jpg "comando tree")

# 52.Guessing the Ability Name
Si excluye el nombre de la habilidad al autorizar de sus controladores, Laravel hará lo mejor para adivinar el método de política apropiado para llamar. Lo hace creando un mapa para las acciones típicas del controlador de descanso y sus métodos de política asociados.

![coamdo tree](64.jpg "comando tree")


# 53.Middleware-Based Authorization

Si prefiere no ejecutar la autorización desde las acciones de su controlador, puede manejarlo como un middleware específico de ruta. Te mostraré cómo en este episodio.

![coamdo tree](65.jpg "comando tree")
![coamdo tree](66.jpg "comando tree")


# 54.Roles and Abilities
Comenzando con una nueva instalación de Laravel, construyamos un sistema de autorización completo basado en roles que nos permita otorgar y revocar dinámicamente varias habilidades por usuario.
![coamdo tree](67.jpg "comando tree")
![coamdo tree](68.jpg "comando tree")
